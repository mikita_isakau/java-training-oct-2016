 /*
 * Copyright (c) 2016, Mikita Isakau. All rights reserved.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package by.epam.training;

import org.apache.tools.ant.Task;
import org.apache.tools.ant.BuildException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;


/**
 * The type Validation task.
 * This task validate the xml-build files
 *
 * @author: Mikita Isakau
 * @see org.apache.tools.ant.Task
 * @see org.apache.tools.ant.BuildException
 * @version 1.02
 */
public class ValidationTask extends Task {

    /**
     * This flag specifies to check dependencies
     */
    private boolean checkDepends;

    /**
     * This flag specifies to check default run target
     */
    private boolean checkDefaults;

    /**
     * This flag specifies to check for correct name
     */
    private boolean checkNames;

    /**
     * This validator using simple SAX-parser for best task's performance
     */
    private SAXParserFactory parserFactory;

    /**
     * The list of XML-build files
     */
    private List<BuildFile> fileList;

    /**
     * Instantiates a new Validation task.
     */
    public ValidationTask() {
        super();
        parserFactory = SAXParserFactory.newInstance();
        fileList = new ArrayList<>();
    }

    /**
     * Sets check defaults.
     * @param flag the check defaults
     */
    public final void setCheckDefaults(final boolean flag) {
        this.checkDefaults = flag;
    }

    /**
     * Sets check depends.
     * @param flag the check depends
     */
    public final void setCheckDepends(final boolean flag) {
        this.checkDepends = flag;
    }

    /**
     * Sets check names.
     * @param flag the check names
     */
    public final void setCheckNames(final boolean flag) {
        this.checkNames = flag;
    }

    @Override
    public final void execute() throws BuildException {
        try {
           SAXParser saxParser = parserFactory.newSAXParser();
           for (BuildFile file : fileList) {
               if (Objects.nonNull(file)) {
                   if (!file.getName().isEmpty()) {
                       saxParser.parse(new File(file.getName()),
                               new ValidatorHandler(file.getName()));
                   }
               }
           }
       } catch (ParserConfigurationException
               | IOException
               | SAXException e) {
            throw new BuildException(e);
       }
    }

    /**
     * Adding build file in handler.
     * @return the build file
     */
    public final BuildFile createBuildFile() {
        BuildFile file = new BuildFile();
        fileList.add(file);
        return file;
    }

    /**
     * The type Build file.
     * This inner class is a inner tag in build file
     */
    public class BuildFile {

        /**
         * Instantiates a new Build file.
         */
        public BuildFile() {
        }

        /**
         * This is a name of build file
         */
        private String name;

        /**
         * Gets name of build file.
         * @return the name
         */
        public final String getName() {
            return name;
        }

        /**
         * Sets name of build file.
         * @param currentName the name of build file
         */
        public final void setName(final String currentName) {
            this.name = currentName;
        }
    }


    private final class ValidatorHandler extends DefaultHandler {
        /**
         * Name of main target
         */
        private static final String MAIN_FIELD = "run";

        /**
         * Name of build file
         */
        private String fileName;

        /**
         * Pattern for matching unknown characters
         */
        private final Pattern pattern = Pattern.compile("[a-zA-Z-]*");

        /**
         * Matcher for find unknown characters
         */
        private Matcher matcher;

        /**
         * This flag shows the correct default target
         */
        private boolean isDefault = false;

        /**
         * This flag shows the correct names
         */
        private boolean isNamedCorrect = true;

        /**
         * This flag shows the correct dependencies
         */
        private boolean isDependsCorrect = true;

        /**
         * @param file - name of build file
         */
        private ValidatorHandler(final String file) {
            this.fileName = file;
        }

        @Override
        public void startDocument() throws SAXException { }

        @Override
        public void endDocument() throws SAXException {
            log("Result of parsing file "
                    + fileName.toUpperCase() + " :");
            if (checkDefaults) {
                log("Default task is valid : "
                        + isDefault  + "!");
            }
            if (checkDepends) {
                log("Depends attributes of main"
                        + " target is valid : "
                        + isDependsCorrect  + "!");
            }
            if (checkNames) {
                log("Names of elements is valid : "
                        + isNamedCorrect  + "!");
            }
            log("_______________________________________");
        }

        @Override
        public void startElement(final String namespace,
                                 final String localName,
                                 final String qName,
                                 final Attributes attr) throws SAXException {
            if ("project".equals(qName)) {
                final String defaultValue = attr.getValue("default");
                if (Objects.nonNull(defaultValue) && !defaultValue.isEmpty()) {
                    ValidatorHandler.this.isDefault = true;
                }
            }
            if ("target".equals(qName)) {
                final String depends = attr.getValue("depends");
                if (MAIN_FIELD.equals(depends)) {
                    ValidatorHandler.this.isDependsCorrect = false;
                }
            }

            final String name = attr.getValue("name");
            if (Objects.nonNull(name) && !name.isEmpty()) {
                matcher = pattern.matcher(name);
                if (!matcher.find()) {
                    isNamedCorrect = false;
                }
            }
        }
        @Override
        public void endElement(final String namespace,
                               final String localName,
                               final String qName) throws SAXException {
        }
        @Override
        public void characters(final char[] ch,
                               final int start,
                               final int end) throws SAXException { }
    }
}