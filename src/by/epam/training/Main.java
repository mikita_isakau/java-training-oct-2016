 /*
* Copyright (c) 2016, Mikita Isakau. All rights reserved.
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
*/
package by.epam.training;

 /**
  * The type Main - first point to run application.
  */
public final class Main {
	 private Main() { }

	 public static void main(final String[] args) {
		 System.out.println("Hello from by.epam.training.Main Class!!");
	}
}